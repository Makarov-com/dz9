const AddTodo = ({ onAdd }) => {
    return (<>
        <input id="add-todo" onKeyPress={e => {
            if(e.charCode === 13){
                onAdd(e.target.value)

            }
        }} />
        <label htmlFor="add-todo">{"Add New Todo"}</label>
    </>);
}

export default AddTodo;
